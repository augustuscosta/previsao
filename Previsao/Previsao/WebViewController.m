//
//  WebViewController.m
//  Previsao
//
//  Created by Nichollas Fonseca on 04/02/13.
//  Copyright (c) 2013 Nichollas Fonseca. All rights reserved.
//

#import "WebViewController.h"

#define SERVER_URL @"http://appbrt.m2mfrota.com/"

@interface WebViewController ()

@end

@implementation WebViewController

@synthesize imageView = _imageView;
@synthesize webView = _webView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self.webView setHidden:YES];
    
    
    if ([self isIpad]){
        portraitImage = [UIImage imageNamed:@"splashIpadPortrait.png"];
        landscapeImage = [UIImage imageNamed:@"splashIpadLandscape.png"];
    }
    else{
        portraitImage = [UIImage imageNamed:@"splashIphonePortrait.png"];
        landscapeImage = [UIImage imageNamed:@"splashIphoneLandscape.png"];
    }
    if ([self isPortraitOrientation])
        [self.imageView setImage:portraitImage];
    else
        [self.imageView setImage:landscapeImage];
    
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
	[self.view addSubview:HUD];
	HUD.delegate = self;
    
    [self loadWebViewRequest];
}

-(void)loadWebViewRequest{
    [HUD show:YES];
    NSString *fullURL = SERVER_URL;
    NSURL *url = [NSURL URLWithString:fullURL];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    [self.webView loadRequest:requestObj];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    [webView.scrollView setContentSize: CGSizeMake(webView.frame.size.width, webView.scrollView.contentSize.height)];
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    [HUD removeFromSuperview];
    [self.webView setHidden:NO];
    [self.imageView setHidden:YES];
    
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    UIAlertView *alert = [[UIAlertView alloc]
                          initWithTitle: @"Sem Internet"
                          message: @"Houve problemas de conexão com a internet e a aplicação será encerrada."
                          delegate: self
                          cancelButtonTitle:@"Ok"
                          otherButtonTitles:nil];
    [alert show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
	if (buttonIndex == 0) {
		NSLog(@"user pressed OK");
        exit(0);
	}
}

-(void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
    if(toInterfaceOrientation == UIInterfaceOrientationPortrait || toInterfaceOrientation == UIInterfaceOrientationPortraitUpsideDown)
        [self.imageView setImage:portraitImage];
    else
        [self.imageView setImage:landscapeImage];
}

- (BOOL) isPortraitOrientation{
    if([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortrait || [UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortraitUpsideDown)  return 1;
    return 0;
}

-(BOOL)isIpad{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
        return NO;
    else
        return YES;
}

-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation{
    return YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
