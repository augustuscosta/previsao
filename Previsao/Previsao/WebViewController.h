//
//  WebViewController.h
//  Previsao
//
//  Created by Nichollas Fonseca on 04/02/13.
//  Copyright (c) 2013 Nichollas Fonseca. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"

@interface WebViewController : UIViewController<UIAlertViewDelegate, MBProgressHUDDelegate>{
    MBProgressHUD *HUD;
    UIImage *landscapeImage;
    UIImage *portraitImage;
}
    
@property (strong, nonatomic) IBOutlet UIImageView *imageView;

@property (strong, nonatomic) IBOutlet UIWebView *webView;
@end
