//
//  main.m
//  Previsao
//
//  Created by Nichollas Fonseca on 04/02/13.
//  Copyright (c) 2013 Nichollas Fonseca. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
